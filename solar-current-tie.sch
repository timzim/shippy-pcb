EESchema Schematic File Version 4
LIBS:shippy-pcb-cache
EELAYER 30 0
EELAYER END
$Descr A 11000 8500
encoding utf-8
Sheet 4 8
Title "Shippy O'Shippian"
Date "2020-05-11"
Rev "1.6"
Comp "Author: Timothy A. Zimmerman"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L shippy-pcb:IRLD120PBF Q?
U 1 1 5E99749C
P 5500 3800
AR Path="/5E97AFFB/5E99749C" Ref="Q?"  Part="1" 
AR Path="/5E9911E4/5E99749C" Ref="Q?"  Part="1" 
AR Path="/5E992E95/5E99749C" Ref="Q501"  Part="1" 
F 0 "Q501" H 5704 3846 50  0000 L CNN
F 1 "??" H 5704 3755 50  0000 L CNN
F 2 "" H 5700 3725 50  0001 L CIN
F 3 "" H 5500 3800 50  0001 L CNN
	1    5500 3800
	1    0    0    -1  
$EndComp
$Comp
L shippy-pcb:IRLD120PBF Q?
U 1 1 5E9974A2
P 5500 4300
AR Path="/5E97AFFB/5E9974A2" Ref="Q?"  Part="1" 
AR Path="/5E9911E4/5E9974A2" Ref="Q?"  Part="1" 
AR Path="/5E992E95/5E9974A2" Ref="Q502"  Part="1" 
F 0 "Q502" H 5704 4254 50  0000 L CNN
F 1 "??" H 5704 4345 50  0000 L CNN
F 2 "" H 5700 4225 50  0001 L CIN
F 3 "" H 5500 4300 50  0001 L CNN
	1    5500 4300
	1    0    0    1   
$EndComp
Wire Wire Line
	5600 4000 5600 4050
Wire Wire Line
	5300 3800 5250 3800
Wire Wire Line
	5250 3800 5250 4300
Wire Wire Line
	5250 4300 5300 4300
$Comp
L Device:R R?
U 1 1 5E9974AE
P 4950 4050
AR Path="/5E97AFFB/5E9974AE" Ref="R?"  Part="1" 
AR Path="/5E9911E4/5E9974AE" Ref="R?"  Part="1" 
AR Path="/5E992E95/5E9974AE" Ref="R501"  Part="1" 
F 0 "R501" H 5020 4096 50  0000 L CNN
F 1 "100k" H 5020 4005 50  0000 L CNN
F 2 "" V 4880 4050 50  0001 C CNN
F 3 "~" H 4950 4050 50  0001 C CNN
	1    4950 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 3800 4950 3800
Connection ~ 5250 3800
Wire Wire Line
	4950 3900 4950 3800
Connection ~ 4950 3800
Wire Wire Line
	4950 3800 5250 3800
$Comp
L power:GND #PWR?
U 1 1 5E9974B9
P 4950 4300
AR Path="/5E97AFFB/5E9974B9" Ref="#PWR?"  Part="1" 
AR Path="/5E9911E4/5E9974B9" Ref="#PWR?"  Part="1" 
AR Path="/5E992E95/5E9974B9" Ref="#PWR0115"  Part="1" 
F 0 "#PWR0115" H 4950 4050 50  0001 C CNN
F 1 "GND" H 4955 4127 50  0000 C CNN
F 2 "" H 4950 4300 50  0001 C CNN
F 3 "" H 4950 4300 50  0001 C CNN
	1    4950 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 4200 4950 4300
Text HLabel 4750 3800 0    50   Input ~ 0
SOLAR_TIE_EN
Text HLabel 5700 3450 2    50   Input ~ 0
SOLAR_RAIL_1
Text HLabel 5700 4650 2    50   Input ~ 0
SOLAR_RAIL_2
Wire Wire Line
	5700 3450 5600 3450
Wire Wire Line
	5600 3450 5600 3600
Wire Wire Line
	5600 4500 5600 4650
Wire Wire Line
	5600 4650 5700 4650
Text HLabel 5700 4050 2    50   Input ~ 0
SOLAR_RAIL_12
Wire Wire Line
	5600 4050 5700 4050
Connection ~ 5600 4050
Wire Wire Line
	5600 4050 5600 4100
$EndSCHEMATC
